package com.autofocus.www.kerent.ui.base.view

interface MVPView {

    fun showProgress()

    fun hideProgress()

}