package com.autofocus.www.kerent.ui.menu.auth.view

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.MenuItem
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.ui.base.view.BaseActivity
import com.autofocus.www.kerent.ui.menu.auth.interactor.AuthMVPInteractor
import com.autofocus.www.kerent.ui.menu.auth.presenter.AuthMVPPresenter
import com.autofocus.www.kerent.ui.menu.home.view.HomeFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class AuthMVPActivity : BaseActivity(), AuthMVPView,
        HasSupportFragmentInjector {

    @Inject
    internal lateinit var presenter: AuthMVPPresenter<AuthMVPView, AuthMVPInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun openLoginActivity() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var firsttime = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

}
