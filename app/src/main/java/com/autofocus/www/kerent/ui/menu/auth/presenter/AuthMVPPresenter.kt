package com.autofocus.www.kerent.ui.menu.auth.presenter

import com.autofocus.www.kerent.ui.base.presenter.MVPPresenter
import com.autofocus.www.kerent.ui.menu.auth.interactor.AuthMVPInteractor
import com.autofocus.www.kerent.ui.menu.auth.view.AuthMVPView

interface AuthMVPPresenter<V : AuthMVPView, I : AuthMVPInteractor> : MVPPresenter<V, I> {}