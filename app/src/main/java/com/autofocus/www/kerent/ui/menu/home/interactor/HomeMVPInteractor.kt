package com.autofocus.www.kerent.ui.menu.home.interactor

import com.autofocus.www.kerent.data.network.HomeResponse
import com.autofocus.www.kerent.ui.base.interactor.MVPInteractor
import io.reactivex.Observable


interface HomeMVPInteractor : MVPInteractor {

    fun getHomeData(): Observable<HomeResponse.Response>
}