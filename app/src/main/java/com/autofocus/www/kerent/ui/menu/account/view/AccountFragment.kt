package com.autofocus.www.kerent.ui.menu.account.view

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.ui.base.view.BaseFragment
import com.autofocus.www.kerent.ui.menu.account.interactor.AccountMVPInteractor
import com.autofocus.www.kerent.ui.menu.account.presenter.AccountMVPPresenter
import javax.inject.Inject

class AccountFragment : BaseFragment(), AccountMVPView {

    companion object {

        fun newInstance(): AccountFragment {
            return AccountFragment()
        }
    }

//    @Inject
//    internal lateinit var AccountAdapter: AccountAdapter
//    @Inject
//    internal lateinit var layoutManager: LinearLayoutManager
    @Inject
    internal lateinit var presenter: AccountMVPPresenter<AccountMVPView, AccountMVPInteractor>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_account, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun setUp() {
//        layoutManager.orientation = LinearLayoutManager.VERTICAL
//        Account_recycler_view.layoutManager = layoutManager
//        Account_recycler_view.itemAnimator = DefaultItemAnimator()
//        Account_recycler_view.adapter = AccountAdapter
        presenter.onViewPrepared()
    }

//    override fun displayAccountList(Accounts: List<Account>?) = Accounts?.let {
//        AccountAdapter.addAccountsToList(it)
//    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }
}