package com.autofocus.www.kerent.ui.menu.account

import android.support.v7.widget.LinearLayoutManager
import com.autofocus.www.kerent.ui.menu.account.interactor.AccountInteractor
import com.autofocus.www.kerent.ui.menu.account.interactor.AccountMVPInteractor
import com.autofocus.www.kerent.ui.menu.account.presenter.AccountMVPPresenter
import com.autofocus.www.kerent.ui.menu.account.presenter.AccountPresenter
import com.autofocus.www.kerent.ui.menu.account.view.AccountMVPView
//import com.autofocus.www.kerent.ui.menu.Account.view.AccountAdapter
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class AccountFragmentModule {

    @Provides
    internal fun provideAccountInteractor(interactor: AccountInteractor): AccountMVPInteractor = interactor

    @Provides
    internal fun provideAccountPresenter(presenter: AccountPresenter<AccountMVPView, AccountMVPInteractor>)
            : AccountMVPPresenter<AccountMVPView, AccountMVPInteractor> = presenter

//    @Provides
//    internal fun provideAccountAdapter(): AccountAdapter = AccountAdapter(ArrayList())

//    @Provides
//    internal fun provideLinearLayoutManager(fragment: AccountFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)

}