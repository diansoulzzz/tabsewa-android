package com.autofocus.www.kerent.di

import javax.inject.Qualifier

@Qualifier
@Retention annotation class ApiKeyInfo