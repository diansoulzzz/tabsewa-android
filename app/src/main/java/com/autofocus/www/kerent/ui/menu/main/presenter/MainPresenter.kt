package com.autofocus.www.kerent.ui.menu.main.presenter

import com.autofocus.www.kerent.ui.base.presenter.BasePresenter
import com.autofocus.www.kerent.ui.menu.main.interactor.MainMVPInteractor
import com.autofocus.www.kerent.ui.menu.main.view.MainMVPView
import com.autofocus.www.kerent.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MainPresenter<V : MainMVPView, I : MainMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), MainMVPPresenter<V, I> {

}
