package com.autofocus.www.kerent.ui.menu.account

import com.autofocus.www.kerent.ui.menu.account.view.AccountFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class AccountFragmentProvider {

    @ContributesAndroidInjector(modules = [AccountFragmentModule::class])
    internal abstract fun provideAccountFragmentFactory(): AccountFragment
}