package com.autofocus.www.kerent.data.database.repository.unused.barang

import io.reactivex.Observable
import javax.inject.Inject

class BarangRepository @Inject internal constructor(private val barangDao: BarangDao) : BarangRepo {

    override fun isBarangRepoEmpty(): Observable<Boolean> = Observable.fromCallable({ barangDao.loadAll().isEmpty() })

    override fun insertBarang(barangs: List<Barang>): Observable<Boolean> {
        barangDao.insertAll(barangs)
        return Observable.just(true)
    }

    override fun loadBarang(): Observable<List<Barang>> = Observable.fromCallable({ barangDao.loadAll() })
}


