package com.autofocus.www.kerent.ui.menu.splash.view

import com.autofocus.www.kerent.ui.base.view.MVPView

interface SplashMVPView : MVPView {
    fun showSuccessToast()
    fun showErrorToast()
    fun openMainActivity()
    fun openLoginActivity()
}