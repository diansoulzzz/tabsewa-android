package com.autofocus.www.kerent.ui.menu.account.presenter

import com.autofocus.www.kerent.ui.base.presenter.MVPPresenter
import com.autofocus.www.kerent.ui.menu.account.interactor.AccountMVPInteractor
import com.autofocus.www.kerent.ui.menu.account.view.AccountMVPView

interface AccountMVPPresenter<V : AccountMVPView, I : AccountMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()
}