package com.autofocus.www.kerent.ui.base.presenter

import com.autofocus.www.kerent.ui.base.interactor.MVPInteractor
import com.autofocus.www.kerent.ui.base.view.MVPView

interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}