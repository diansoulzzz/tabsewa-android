package com.autofocus.www.kerent.ui.menu.home.presenter

import android.util.Log
import com.autofocus.www.kerent.ui.base.presenter.BasePresenter
import com.autofocus.www.kerent.ui.menu.home.interactor.HomeMVPInteractor
import com.autofocus.www.kerent.ui.menu.home.view.HomeMVPView
import com.autofocus.www.kerent.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class HomePresenter<V : HomeMVPView, I : HomeMVPInteractor> @Inject constructor(interactor: I, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = compositeDisposable), HomeMVPPresenter<V, I> {

    override fun onViewPrepared() {
        getView()?.showProgress()
        Log.d("AppLogs","onViewPrepared")
        interactor?.let {
            it.getHomeData()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe ({ response ->
                        Log.d("AppLogs","onViewPrepared.subscribe")
                        getView()?.let {
                            it.hideProgress()
                            it.displayHomeData(response)
                            Log.d("AppLogs","onViewPrepared.getView")
                        }
                    }, { err ->
//                        println(err)
                        Log.d("AppLogs","onViewPrepared.Error : " +err.message)
                    })
        }
    }
}