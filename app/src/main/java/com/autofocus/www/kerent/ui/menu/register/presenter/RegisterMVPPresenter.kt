package com.autofocus.www.kerent.ui.menu.register.presenter

import com.autofocus.www.kerent.ui.base.presenter.MVPPresenter
import com.autofocus.www.kerent.ui.menu.register.interactor.RegisterMVPInteractor
import com.autofocus.www.kerent.ui.menu.register.view.RegisterMVPView

interface RegisterMVPPresenter<V : RegisterMVPView, I : RegisterMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()
}