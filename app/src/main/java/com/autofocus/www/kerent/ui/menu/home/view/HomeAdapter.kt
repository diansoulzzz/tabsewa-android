package com.autofocus.www.kerent.ui.menu.home.view

import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.data.models.Barang

class HomeAdapter(private val barangList: MutableList<Barang>) : RecyclerView.Adapter<HomeAdapter.HomeViewHolder>() {

    override fun getItemCount() = this.barangList.size

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) = holder.let {
        it.clear()
        it.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder = HomeViewHolder(LayoutInflater.from(parent?.context)
            .inflate(R.layout.item_barang, parent, false))

    internal fun addBarangToList(barangs: List<Barang>) {
        this.barangList.addAll(barangs)
        notifyDataSetChanged()
    }

    inner class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun clear() {
//            itemView.coverImageView.setImageDrawable(null)
//            itemView.titleTextView.text = ""
//            itemView.contentTextView.text = ""
        }

        fun onBind(position: Int) {
            val (title, coverPageUrl, date, description, author, blogUrl) = barangList[position]

//            inflateData(title, author, date, description, coverPageUrl)
//            setItemClickListener(blogUrl)
        }

        private fun setItemClickListener(blogUrl: String?) {
            itemView.setOnClickListener {
                blogUrl?.let {
                    try {
                        val intent = Intent()
                        // using "with" as an example
                        with(intent) {
                            action = Intent.ACTION_VIEW
                            data = Uri.parse(it)
                            addCategory(Intent.CATEGORY_BROWSABLE)
                        }
                        itemView.context.startActivity(intent)
                    } catch (e: Exception) {
                    }
                }

            }
        }

        private fun inflateData(title: String?, author: String?, date: String?, description: String?, coverPageUrl: String?) {
//            title?.let { itemView.titleTextView.text = it }
//            author?.let { itemView.authorTextView.text = it }
//            date?.let { itemView.dateTextView.text = it }
//            description?.let { itemView.contentTextView.text = it }
//            coverPageUrl?.let {
//                itemView.coverImageView.loadImage(it)
//            }
        }

    }
}
