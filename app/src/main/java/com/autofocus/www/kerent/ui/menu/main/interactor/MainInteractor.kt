package com.autofocus.www.kerent.ui.menu.main.interactor

import android.content.Context
import com.autofocus.www.kerent.data.network.ApiHelper
import com.autofocus.www.kerent.data.preferences.PreferenceHelper
import com.autofocus.www.kerent.ui.base.interactor.BaseInteractor
import javax.inject.Inject


class MainInteractor @Inject constructor(private val mContext: Context, preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), MainMVPInteractor {

    override fun getData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
