package com.autofocus.www.kerent.ui.menu.register.view

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.ui.base.view.BaseFragment
import com.autofocus.www.kerent.ui.menu.register.interactor.RegisterMVPInteractor
import com.autofocus.www.kerent.ui.menu.register.presenter.RegisterMVPPresenter
import javax.inject.Inject

class RegisterFragment : BaseFragment(), RegisterMVPView {

    companion object {

        fun newInstance(): RegisterFragment {
            return RegisterFragment()
        }
    }

//    @Inject
//    internal lateinit var RegisterAdapter: RegisterAdapter
//    @Inject
//    internal lateinit var layoutManager: LinearLayoutManager
    @Inject
    internal lateinit var presenter: RegisterMVPPresenter<RegisterMVPView, RegisterMVPInteractor>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_register, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun setUp() {
//        layoutManager.orientation = LinearLayoutManager.VERTICAL
//        Register_recycler_view.layoutManager = layoutManager
//        Register_recycler_view.itemAnimator = DefaultItemAnimator()
//        Register_recycler_view.adapter = RegisterAdapter
        presenter.onViewPrepared()
    }

//    override fun displayRegisterList(Registers: List<Register>?) = Registers?.let {
//        RegisterAdapter.addRegistersToList(it)
//    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }
}