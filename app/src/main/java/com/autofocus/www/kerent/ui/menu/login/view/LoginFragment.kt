package com.autofocus.www.kerent.ui.menu.login.view

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.ui.base.view.BaseFragment
import com.autofocus.www.kerent.ui.menu.login.interactor.LoginMVPInteractor
import com.autofocus.www.kerent.ui.menu.login.presenter.LoginMVPPresenter
import javax.inject.Inject

class LoginFragment : BaseFragment(), LoginMVPView {

    companion object {

        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

//    @Inject
//    internal lateinit var LoginAdapter: LoginAdapter
//    @Inject
//    internal lateinit var layoutManager: LinearLayoutManager
    @Inject
    internal lateinit var presenter: LoginMVPPresenter<LoginMVPView, LoginMVPInteractor>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_login, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun setUp() {
//        layoutManager.orientation = LinearLayoutManager.VERTICAL
//        Login_recycler_view.layoutManager = layoutManager
//        Login_recycler_view.itemAnimator = DefaultItemAnimator()
//        Login_recycler_view.adapter = LoginAdapter
        presenter.onViewPrepared()
    }

//    override fun displayLoginList(Logins: List<Login>?) = Logins?.let {
//        LoginAdapter.addLoginsToList(it)
//    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }
}