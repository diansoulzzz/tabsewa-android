package com.autofocus.www.kerent.data.database.repository.unused.users

import io.reactivex.Observable
import javax.inject.Inject


class UserRepository @Inject internal constructor(private val userDao: UserDao) : UserRepo {

    override fun isUserRepoEmpty(): Observable<Boolean> = Observable.fromCallable({ userDao.loadAll().isEmpty() })

    override fun insertUser(users: List<User>): Observable<Boolean> {
        userDao.insertAll(users)
        return Observable.just(true)
    }

    override fun loadUser(): Observable<List<User>> = Observable.fromCallable({ userDao.loadAll() })
}


