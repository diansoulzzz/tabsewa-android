package com.autofocus.www.kerent.data.database.repository.unused.barang_kategori

import android.arch.persistence.room.ColumnInfo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BarangKategori(
        @Expose @SerializedName("id") @ColumnInfo(name = "id") var id: Int,
        @Expose @SerializedName("nama") @ColumnInfo(name = "nama") var nama: String,
        @Expose @SerializedName("foto_url") @ColumnInfo(name = "foto_url") var fotoUrl: String,
        @Expose @SerializedName("created_at") @ColumnInfo(name = "created_at") var createdAt: String,
        @Expose @SerializedName("updated_at") @ColumnInfo(name = "updated_at") var updatedAt: String,
        @Expose @SerializedName("deleted_at") @ColumnInfo(name = "deleted_at") var deletedAt: Any,
        @Expose @SerializedName("users_id") @ColumnInfo(name = "users_id") var usersId: Int
)