package com.autofocus.www.kerent.ui.menu.home.interactor

import com.autofocus.www.kerent.data.network.ApiHelper
import com.autofocus.www.kerent.data.network.HomeResponse
import com.autofocus.www.kerent.data.preferences.PreferenceHelper
import com.autofocus.www.kerent.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class HomeInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), HomeMVPInteractor {

    override fun getHomeData() = apiHelper.getHomeDataApiCall()

}