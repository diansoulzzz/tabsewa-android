package com.autofocus.www.kerent.ui.menu.login

import com.autofocus.www.kerent.ui.menu.login.view.LoginFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class LoginFragmentProvider {

    @ContributesAndroidInjector(modules = [LoginFragmentModule::class])
    internal abstract fun provideLoginFragmentFactory(): LoginFragment
}