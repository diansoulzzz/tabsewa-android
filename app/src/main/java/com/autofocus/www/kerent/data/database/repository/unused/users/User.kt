package com.autofocus.www.kerent.data.database.repository.unused.users


import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "users")
data class User(
        @Expose @PrimaryKey var id: Long,
        @Expose @SerializedName("name") @ColumnInfo(name = "name") var name: String?,
        @Expose @SerializedName("email") @ColumnInfo(name = "email") var email: String?,
        @Expose @SerializedName("foto_url") @ColumnInfo(name = "foto_url") var fotoUrl: String?,
        @Expose @SerializedName("created_at") @ColumnInfo(name = "created_at") var createdAt: String?,
        @Expose @SerializedName("updated_at") @ColumnInfo(name = "updated_at") var updatedAt: String?,
        @Expose @SerializedName("aktif") @ColumnInfo(name = "aktif") var aktif: Int?,
        @Expose @SerializedName("provider") @ColumnInfo(name = "provider") var provider: String?,
        @Expose @SerializedName("provider_id") @ColumnInfo(name = "provider_id") var providerId: String?,
        @Expose @SerializedName("verified") @ColumnInfo(name = "verified") var verified: Int?,
        @Expose @SerializedName("no_ktp") @ColumnInfo(name = "no_ktp") var noKtp: String?,
        @Expose @SerializedName("foto_ktp") @ColumnInfo(name = "foto_ktp") var fotoKtp: String?,
        @Expose @SerializedName("firebase_token") @ColumnInfo(name = "firebase_token") var firebaseToken: String?
)