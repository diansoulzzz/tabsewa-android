package com.autofocus.www.kerent.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BarangKategori(
        @Expose @SerializedName("id") var id: Int,
        @Expose @SerializedName("nama") var nama: String,
        @Expose @SerializedName("foto_url") var fotoUrl: String,
        @Expose @SerializedName("created_at") var createdAt: String,
        @Expose @SerializedName("updated_at") var updatedAt: String,
        @Expose @SerializedName("deleted_at") var deletedAt: Any,
        @Expose @SerializedName("users_id") var usersId: Int
)