package com.autofocus.www.kerent.ui.menu.splash

import com.autofocus.www.kerent.ui.menu.splash.interactor.SplashInteractor
import com.autofocus.www.kerent.ui.menu.splash.interactor.SplashMVPInteractor
import com.autofocus.www.kerent.ui.menu.splash.presenter.SplashMVPPresenter
import com.autofocus.www.kerent.ui.menu.splash.presenter.SplashPresenter
import com.autofocus.www.kerent.ui.menu.splash.view.SplashMVPView
import dagger.Module
import dagger.Provides

@Module
class SplashActivityModule {
    @Provides
    internal fun provideSplashInteractor(splashInteractor: SplashInteractor): SplashMVPInteractor = splashInteractor

    @Provides
    internal fun provideSplashPresenter(splashPresenter: SplashPresenter<SplashMVPView, SplashMVPInteractor>)
            : SplashMVPPresenter<SplashMVPView, SplashMVPInteractor> = splashPresenter
}