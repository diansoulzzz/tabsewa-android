package com.autofocus.www.kerent.ui.menu.auth.view

import com.autofocus.www.kerent.ui.base.view.MVPView

interface AuthMVPView : MVPView {
    fun openLoginActivity()
}