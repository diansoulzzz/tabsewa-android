package com.autofocus.www.kerent.ui.menu.auth.presenter

import com.autofocus.www.kerent.ui.base.presenter.BasePresenter
import com.autofocus.www.kerent.ui.menu.auth.interactor.AuthMVPInteractor
import com.autofocus.www.kerent.ui.menu.auth.view.AuthMVPView
import com.autofocus.www.kerent.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class AuthPresenter<V : AuthMVPView, I : AuthMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), AuthMVPPresenter<V, I> {

}
