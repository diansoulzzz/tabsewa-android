package com.autofocus.www.kerent.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.autofocus.www.kerent.BuildConfig
import com.autofocus.www.kerent.data.database.AppDatabase
import com.autofocus.www.kerent.data.database.repository.questions.QuestionRepo
import com.autofocus.www.kerent.data.database.repository.questions.QuestionRepository
import com.autofocus.www.kerent.data.network.ApiHeader
import com.autofocus.www.kerent.data.network.ApiHelper
import com.autofocus.www.kerent.data.network.AppApiHelper
import com.autofocus.www.kerent.data.preferences.AppPreferenceHelper
import com.autofocus.www.kerent.data.preferences.PreferenceHelper
import com.autofocus.www.kerent.di.ApiKeyInfo
import com.autofocus.www.kerent.di.PreferenceInfo
import com.autofocus.www.kerent.util.AppConstants
import com.autofocus.www.kerent.util.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideAppDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, AppConstants.APP_DB_NAME).build()

    @Provides
    @ApiKeyInfo
    internal fun provideApiKey(): String = BuildConfig.API_KEY

    @Provides
    @PreferenceInfo
    internal fun provideprefFileName(): String = AppConstants.PREF_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    @Singleton
    internal fun provideProtectedApiHeader(@ApiKeyInfo apiKey: String, preferenceHelper: PreferenceHelper)
            : ApiHeader.ProtectedApiHeader = ApiHeader.ProtectedApiHeader(apiKey = apiKey,
            userId = preferenceHelper.getCurrentUserId(),
            accessToken = preferenceHelper.getAccessToken())

    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper

    @Provides
    @Singleton
    internal fun provideQuestionRepoHelper(appDatabase: AppDatabase): QuestionRepo = QuestionRepository(appDatabase.questionsDao())

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()


}