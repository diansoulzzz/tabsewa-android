package com.autofocus.www.kerent.di.builder

import com.autofocus.www.kerent.ui.menu.account.AccountFragmentProvider
import com.autofocus.www.kerent.ui.menu.auth.AuthActivityModule
import com.autofocus.www.kerent.ui.menu.auth.view.AuthMVPActivity
import com.autofocus.www.kerent.ui.menu.category.CategoryFragmentProvider
import com.autofocus.www.kerent.ui.menu.home.HomeFragmentProvider
import com.autofocus.www.kerent.ui.menu.login.LoginFragmentProvider
import com.autofocus.www.kerent.ui.menu.main.MainActivityModule
import com.autofocus.www.kerent.ui.menu.main.view.MainMVPActivity
import com.autofocus.www.kerent.ui.menu.register.RegisterFragmentProvider
import com.autofocus.www.kerent.ui.menu.splash.SplashActivityModule
import com.autofocus.www.kerent.ui.menu.splash.view.SplashMVPActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashMVPActivity

    @ContributesAndroidInjector(modules = [(MainActivityModule::class),(HomeFragmentProvider::class),(CategoryFragmentProvider::class),(AccountFragmentProvider::class)])
    abstract fun bindMainctivity(): MainMVPActivity

    @ContributesAndroidInjector(modules = [(AuthActivityModule::class),(LoginFragmentProvider::class),(RegisterFragmentProvider::class)])
    abstract fun bindAuthActivity(): AuthMVPActivity
}