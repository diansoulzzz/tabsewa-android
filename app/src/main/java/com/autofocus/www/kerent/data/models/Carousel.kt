package com.autofocus.www.kerent.data.models

import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Carousel(
        @Expose @PrimaryKey var id: Long,
        @Expose @SerializedName("nama") var nama: String,
        @Expose @SerializedName("keterangan") var keterangan: String?,
        @Expose @SerializedName("tgl_awal") var tglAwal: String?,
        @Expose @SerializedName("tgl_akhir") var tglAkhir: String?,
        @Expose @SerializedName("foto_url") var fotoUrl: String?,
        @Expose @SerializedName("created_at") var createdAt: String?,
        @Expose @SerializedName("updated_at") var updatedAt: String?,
        @Expose @SerializedName("deleted_at") var deletedAt: String?,
        @Expose @SerializedName("users_id") var usersId: Int
)