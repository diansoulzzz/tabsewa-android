package com.autofocus.www.kerent.data.network

import io.reactivex.Observable

interface ApiHelper {

    fun performServerLogin(request: LoginRequest.ServerLoginRequest): Observable<LoginResponse>

    fun getHomeDataApiCall(): Observable<HomeResponse.Response>
}