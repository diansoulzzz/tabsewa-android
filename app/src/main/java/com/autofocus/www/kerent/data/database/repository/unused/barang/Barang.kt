package com.autofocus.www.kerent.data.database.repository.unused.barang

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "barang")
data class Barang(
        @Expose @SerializedName("id") @ColumnInfo(name = "id") var id: Int,
        @Expose @SerializedName("nama") @ColumnInfo(name = "nama") var nama: String,
        @Expose @SerializedName("deskripsi") @ColumnInfo(name = "deskripsi") var deskripsi: String,
        @Expose @SerializedName("barang_sub_kategori_id") @ColumnInfo(name = "barang_sub_kategori_id") var barangSubKategoriId: Int,
        @Expose @SerializedName("users_id") @ColumnInfo(name = "users_id") var usersId: Int,
        @Expose @SerializedName("stok") @ColumnInfo(name = "stok") var stok: Int,
        @Expose @SerializedName("created_at") @ColumnInfo(name = "created_at") var createdAt: String,
        @Expose @SerializedName("updated_at") @ColumnInfo(name = "updated_at") var updatedAt: String,
        @Expose @SerializedName("deleted_at") @ColumnInfo(name = "deleted_at") var deletedAt: Any,
        @Expose @SerializedName("deposit_min") @ColumnInfo(name = "deposit_min") var depositMin: Int,
        @Expose @SerializedName("stok_available") @ColumnInfo(name = "stok_available") var stokAvailable: Int,
        @Expose @SerializedName("stok_onbook") @ColumnInfo(name = "stok_onbook") var stokOnbook: Int
)