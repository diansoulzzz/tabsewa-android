package com.autofocus.www.kerent.ui.menu.splash.view

import android.content.Intent
import android.os.Bundle
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.ui.base.view.BaseActivity
import com.autofocus.www.kerent.ui.menu.main.view.MainMVPActivity
import com.autofocus.www.kerent.ui.menu.splash.interactor.SplashMVPInteractor
import com.autofocus.www.kerent.ui.menu.splash.presenter.SplashMVPPresenter
import javax.inject.Inject

class SplashMVPActivity : BaseActivity(), SplashMVPView {

    @Inject
    lateinit var presenter: SplashMVPPresenter<SplashMVPView, SplashMVPInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun onFragmentAttached() {
    }

    override fun showSuccessToast() {
    }

    override fun showErrorToast() {
    }

    override fun openMainActivity() {
        val intent = Intent(this, MainMVPActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun openLoginActivity() {
        val intent = Intent(this, MainMVPActivity::class.java)
        startActivity(intent)
        finish()
    }
}
