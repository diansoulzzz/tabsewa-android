package com.autofocus.www.kerent.data.database.repository.unused.carousel

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface CarouselDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(question: List<Carousel>)

    @Query("SELECT * FROM carousel")
    fun loadAll(): List<Carousel>
}