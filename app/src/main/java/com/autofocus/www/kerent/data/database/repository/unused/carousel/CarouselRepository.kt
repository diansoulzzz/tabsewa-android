package com.autofocus.www.kerent.data.database.repository.unused.carousel

import io.reactivex.Observable
import javax.inject.Inject


class CarouselRepository @Inject internal constructor(private val carouselDao: CarouselDao) : CarouselRepo {

    override fun isCarouselRepoEmpty(): Observable<Boolean> = Observable.fromCallable({ carouselDao.loadAll().isEmpty() })

    override fun insertCarousel(carousels: List<Carousel>): Observable<Boolean> {
        carouselDao.insertAll(carousels)
        return Observable.just(true)
    }

    override fun loadCarousel(): Observable<List<Carousel>> = Observable.fromCallable({ carouselDao.loadAll() })
}


