package com.autofocus.www.kerent.data.database.repository.unused.users

import io.reactivex.Observable

interface UserRepo {

    fun isUserRepoEmpty(): Observable<Boolean>

    fun insertUser(users: List<User>): Observable<Boolean>

    fun loadUser(): Observable<List<User>>
}