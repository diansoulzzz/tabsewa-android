package com.autofocus.www.kerent.ui.menu.category.view

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.ui.base.view.BaseFragment
import com.autofocus.www.kerent.ui.menu.category.interactor.CategoryMVPInteractor
import com.autofocus.www.kerent.ui.menu.category.presenter.CategoryMVPPresenter
import javax.inject.Inject

class CategoryFragment : BaseFragment(), CategoryMVPView {

    companion object {

        fun newInstance(): CategoryFragment {
            return CategoryFragment()
        }
    }

//    @Inject
//    internal lateinit var CategoryAdapter: CategoryAdapter
//    @Inject
//    internal lateinit var layoutManager: LinearLayoutManager
    @Inject
    internal lateinit var presenter: CategoryMVPPresenter<CategoryMVPView, CategoryMVPInteractor>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_category, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun setUp() {
//        layoutManager.orientation = LinearLayoutManager.VERTICAL
//        Category_recycler_view.layoutManager = layoutManager
//        Category_recycler_view.itemAnimator = DefaultItemAnimator()
//        Category_recycler_view.adapter = CategoryAdapter
        presenter.onViewPrepared()
    }

//    override fun displayCategoryList(Categorys: List<Category>?) = Categorys?.let {
//        CategoryAdapter.addCategorysToList(it)
//    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }
}