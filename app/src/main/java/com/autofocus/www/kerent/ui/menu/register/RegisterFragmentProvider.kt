package com.autofocus.www.kerent.ui.menu.register

import com.autofocus.www.kerent.ui.menu.register.view.RegisterFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class RegisterFragmentProvider {

    @ContributesAndroidInjector(modules = [RegisterFragmentModule::class])
    internal abstract fun provideRegisterFragmentFactory(): RegisterFragment
}