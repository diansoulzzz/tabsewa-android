package com.autofocus.www.kerent.ui.menu.home.view

import com.autofocus.www.kerent.data.network.HomeResponse
import com.autofocus.www.kerent.ui.base.view.MVPView

interface HomeMVPView : MVPView {

//    fun displayBlogList(blogs: List<Blog>?) : Unit?
    fun displayHomeData(home : HomeResponse.Response)

}