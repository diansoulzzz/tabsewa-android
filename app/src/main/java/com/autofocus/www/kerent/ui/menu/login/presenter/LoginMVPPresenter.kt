package com.autofocus.www.kerent.ui.menu.login.presenter

import com.autofocus.www.kerent.ui.base.presenter.MVPPresenter
import com.autofocus.www.kerent.ui.menu.login.interactor.LoginMVPInteractor
import com.autofocus.www.kerent.ui.menu.login.view.LoginMVPView


interface LoginMVPPresenter<V : LoginMVPView, I : LoginMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()
}