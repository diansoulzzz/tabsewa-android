package com.autofocus.www.kerent.ui.menu.splash.presenter

import com.autofocus.www.kerent.ui.base.presenter.MVPPresenter
import com.autofocus.www.kerent.ui.menu.splash.interactor.SplashMVPInteractor
import com.autofocus.www.kerent.ui.menu.splash.view.SplashMVPView


interface SplashMVPPresenter<V : SplashMVPView, I : SplashMVPInteractor> : MVPPresenter<V, I>