package com.autofocus.www.kerent.util.extension

import android.widget.ImageView
import com.bumptech.glide.Glide

internal fun ImageView.loadImage(url: String) {
    Glide.with(this.context)
            .asBitmap()
            .load(url)
            .into(this)
//            .centerCrop()
}