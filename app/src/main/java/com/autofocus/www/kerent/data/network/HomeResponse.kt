package com.autofocus.www.kerent.data.network

import com.autofocus.www.kerent.data.models.Barang
import com.autofocus.www.kerent.data.models.BarangKategori
import com.autofocus.www.kerent.data.models.BarangSubKategori
import com.autofocus.www.kerent.data.models.Carousel
import com.google.gson.annotations.SerializedName

class HomeResponse {
    data class Response(
            @SerializedName("data") var data: Data
    )

    data class Data(
            @SerializedName("barang") var barang: List<Barang>,
            @SerializedName("carousel") var carousel: List<Carousel>,
            @SerializedName("kategori") var kategori: List<BarangKategori>,
            @SerializedName("subkategori") var subkategori: List<BarangSubKategori>
    )
}
