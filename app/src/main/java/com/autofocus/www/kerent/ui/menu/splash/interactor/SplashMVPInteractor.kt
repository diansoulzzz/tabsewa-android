package com.autofocus.www.kerent.ui.menu.splash.interactor

import com.autofocus.www.kerent.data.database.repository.questions.Question
import com.autofocus.www.kerent.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface SplashMVPInteractor : MVPInteractor {
    fun getQuestion() : Observable<List<Question>>
    fun seedQuestions(): Observable<Boolean>
}