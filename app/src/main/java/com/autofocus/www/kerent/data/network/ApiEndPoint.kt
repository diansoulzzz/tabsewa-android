package com.autofocus.www.kerent.data.network

import com.autofocus.www.kerent.BuildConfig

object ApiEndPoint {

    val ENDPOINT_GOOGLE_LOGIN = BuildConfig.BASE_URL + "/588d14f4100000a9072d2943"
    val ENDPOINT_FACEBOOK_LOGIN = BuildConfig.BASE_URL + "/588d15d3100000ae072d2944"
    val ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL + "/588d15f5100000a8072d2945"
    val ENDPOINT_LOGOUT = BuildConfig.BASE_URL + "/588d161c100000a9072d2946"


    val ENDPOINT_HOME_DATA = BuildConfig.BASE_URL + "/home/data"

}