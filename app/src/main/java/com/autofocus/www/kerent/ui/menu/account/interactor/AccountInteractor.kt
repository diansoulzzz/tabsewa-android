package com.autofocus.www.kerent.ui.menu.account.interactor

import com.autofocus.www.kerent.data.network.ApiHelper
import com.autofocus.www.kerent.data.preferences.PreferenceHelper
import com.autofocus.www.kerent.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class AccountInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), AccountMVPInteractor {

//    override fun getBlogList() = apiHelper.getBlogApiCall()

}