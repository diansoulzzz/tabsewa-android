package com.autofocus.www.kerent.ui.menu.main.view

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.MenuItem
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.ui.base.view.BaseActivity
import com.autofocus.www.kerent.ui.menu.account.view.AccountFragment
import com.autofocus.www.kerent.ui.menu.category.view.CategoryFragment
import com.autofocus.www.kerent.ui.menu.home.view.HomeFragment
import com.autofocus.www.kerent.ui.menu.main.interactor.MainMVPInteractor
import com.autofocus.www.kerent.ui.menu.main.presenter.MainMVPPresenter
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainMVPActivity : BaseActivity(), MainMVPView,
        HasSupportFragmentInjector {

    @Inject
    internal lateinit var presenter: MainMVPPresenter<MainMVPView, MainMVPInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun openLoginActivity() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var firsttime = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigationViewEx.enableAnimation(true)
        bottomNavigationViewEx.enableShiftingMode(false)
        bottomNavigationViewEx.enableItemShiftingMode(false)

        val manager = supportFragmentManager
        bottomNavigationViewEx.onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            if (item.itemId == bottomNavigationViewEx.selectedItemId && !firsttime)
                return@OnNavigationItemSelectedListener false
            when (item.itemId) {
                R.id.menu_home -> {
                    val transaction = manager.beginTransaction()
                    transaction.replace(R.id.content, HomeFragment.newInstance()).commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.menu_category -> {
                    val transaction = manager.beginTransaction()
                    transaction.replace(R.id.content, CategoryFragment.newInstance()).commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.menu_notification -> {
                    val transaction = manager.beginTransaction()
                    transaction.replace(R.id.content, CategoryFragment.newInstance()).commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.menu_account -> {
                    val transaction = manager.beginTransaction()
                    transaction.replace(R.id.content, AccountFragment.newInstance()).commit()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
        bottomNavigationViewEx.currentItem = 0
        firsttime = false
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

}
