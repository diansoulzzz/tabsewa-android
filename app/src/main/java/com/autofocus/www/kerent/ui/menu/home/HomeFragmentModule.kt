package com.autofocus.www.kerent.ui.menu.home

import android.support.v7.widget.LinearLayoutManager
import com.autofocus.www.kerent.ui.menu.home.interactor.HomeInteractor
import com.autofocus.www.kerent.ui.menu.home.interactor.HomeMVPInteractor
import com.autofocus.www.kerent.ui.menu.home.presenter.HomeMVPPresenter
import com.autofocus.www.kerent.ui.menu.home.presenter.HomePresenter
//import com.autofocus.www.kerent.ui.menu.home.view.HomeAdapter
import com.autofocus.www.kerent.ui.menu.home.view.HomeMVPView
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class HomeFragmentModule {

    @Provides
    internal fun provideHomeInteractor(interactor: HomeInteractor): HomeMVPInteractor = interactor

    @Provides
    internal fun provideHomePresenter(presenter: HomePresenter<HomeMVPView, HomeMVPInteractor>)
            : HomeMVPPresenter<HomeMVPView, HomeMVPInteractor> = presenter

//    @Provides
//    internal fun provideHomeAdapter(): HomeAdapter = HomeAdapter(ArrayList())

//    @Provides
//    internal fun provideLinearLayoutManager(fragment: HomeFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)

}