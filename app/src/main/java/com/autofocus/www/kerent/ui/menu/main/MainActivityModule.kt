package com.autofocus.www.kerent.ui.menu.main

import com.autofocus.www.kerent.ui.menu.main.interactor.MainInteractor
import com.autofocus.www.kerent.ui.menu.main.interactor.MainMVPInteractor
import com.autofocus.www.kerent.ui.menu.main.presenter.MainMVPPresenter
import com.autofocus.www.kerent.ui.menu.main.presenter.MainPresenter
import com.autofocus.www.kerent.ui.menu.main.view.MainMVPView
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {
    @Provides
    internal fun provideMainInteractor(MainInteractor: MainInteractor): MainMVPInteractor = MainInteractor

    @Provides
    internal fun provideMainPresenter(MainPresenter: MainPresenter<MainMVPView, MainMVPInteractor>)
            : MainMVPPresenter<MainMVPView, MainMVPInteractor> = MainPresenter
}