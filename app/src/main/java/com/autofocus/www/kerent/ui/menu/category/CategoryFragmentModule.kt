package com.autofocus.www.kerent.ui.menu.category

import android.support.v7.widget.LinearLayoutManager
import com.autofocus.www.kerent.ui.menu.category.interactor.CategoryInteractor
import com.autofocus.www.kerent.ui.menu.category.interactor.CategoryMVPInteractor
import com.autofocus.www.kerent.ui.menu.category.presenter.CategoryMVPPresenter
import com.autofocus.www.kerent.ui.menu.category.presenter.CategoryPresenter
import com.autofocus.www.kerent.ui.menu.category.view.CategoryMVPView
//import com.autofocus.www.kerent.ui.menu.Category.view.CategoryAdapter
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class CategoryFragmentModule {

    @Provides
    internal fun provideCategoryInteractor(interactor: CategoryInteractor): CategoryMVPInteractor = interactor

    @Provides
    internal fun provideCategoryPresenter(presenter: CategoryPresenter<CategoryMVPView, CategoryMVPInteractor>)
            : CategoryMVPPresenter<CategoryMVPView, CategoryMVPInteractor> = presenter

//    @Provides
//    internal fun provideCategoryAdapter(): CategoryAdapter = CategoryAdapter(ArrayList())

//    @Provides
//    internal fun provideLinearLayoutManager(fragment: CategoryFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)

}