package com.autofocus.www.kerent.data.database.repository.unused.barang


import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface BarangDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(question: List<Barang>)

    @Query("SELECT * FROM barang")
    fun loadAll(): List<Barang>
}