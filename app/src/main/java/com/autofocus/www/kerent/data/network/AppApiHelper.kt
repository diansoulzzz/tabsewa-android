package com.autofocus.www.kerent.data.network

import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observable
import javax.inject.Inject

class AppApiHelper @Inject constructor(private val apiHeader: ApiHeader) : ApiHelper {

    override fun getHomeDataApiCall(): Observable<HomeResponse.Response> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_HOME_DATA)
                .addHeaders(apiHeader.publicApiHeader)
                .build()
                .getObjectObservable(HomeResponse.Response::class.java)
    }

    override fun performServerLogin(request: LoginRequest.ServerLoginRequest): Observable<LoginResponse> =
            Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                    .addHeaders(apiHeader.publicApiHeader)
                    .addBodyParameter(request)
                    .build()
                    .getObjectObservable(LoginResponse::class.java)

}