package com.autofocus.www.kerent.ui.menu.main.presenter

import com.autofocus.www.kerent.ui.base.presenter.MVPPresenter
import com.autofocus.www.kerent.ui.menu.main.interactor.MainMVPInteractor
import com.autofocus.www.kerent.ui.menu.main.view.MainMVPView

interface MainMVPPresenter<V : MainMVPView, I : MainMVPInteractor> : MVPPresenter<V, I> {}