package com.autofocus.www.kerent.data.database.repository.unused.carousel

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "carousel")
data class Carousel(
        @Expose @PrimaryKey var id: Long,
        @Expose @SerializedName("nama") @ColumnInfo(name = "nama") var nama: String,
        @Expose @SerializedName("keterangan") @ColumnInfo(name = "keterangan") var keterangan: String?,
        @Expose @SerializedName("tgl_awal") @ColumnInfo(name = "tgl_awal") var tglAwal: String?,
        @Expose @SerializedName("tgl_akhir") @ColumnInfo(name = "tgl_akhir") var tglAkhir: String?,
        @Expose @SerializedName("foto_url") @ColumnInfo(name = "foto_url") var fotoUrl: String?,
        @Expose @SerializedName("created_at") @ColumnInfo(name = "created_at") var createdAt: String?,
        @Expose @SerializedName("updated_at") @ColumnInfo(name = "updated_at") var updatedAt: String?,
        @Expose @SerializedName("deleted_at") @ColumnInfo(name = "deleted_at") var deletedAt: String?,
        @Expose @SerializedName("users_id") @ColumnInfo(name = "users_id") var usersId: Int
)