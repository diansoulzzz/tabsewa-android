package com.autofocus.www.kerent.ui.menu.login

import android.support.v7.widget.LinearLayoutManager
//import com.autofocus.www.kerent.ui.menu.Login.view.LoginAdapter
import com.autofocus.www.kerent.ui.menu.login.interactor.LoginInteractor
import com.autofocus.www.kerent.ui.menu.login.interactor.LoginMVPInteractor
import com.autofocus.www.kerent.ui.menu.login.presenter.LoginMVPPresenter
import com.autofocus.www.kerent.ui.menu.login.presenter.LoginPresenter
import com.autofocus.www.kerent.ui.menu.login.view.LoginMVPView
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class LoginFragmentModule {

    @Provides
    internal fun provideLoginInteractor(interactor: LoginInteractor): LoginMVPInteractor = interactor

    @Provides
    internal fun provideLoginPresenter(presenter: LoginPresenter<LoginMVPView, LoginMVPInteractor>)
            : LoginMVPPresenter<LoginMVPView, LoginMVPInteractor> = presenter

//    @Provides
//    internal fun provideLoginAdapter(): LoginAdapter = LoginAdapter(ArrayList())

//    @Provides
//    internal fun provideLinearLayoutManager(fragment: LoginFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)

}