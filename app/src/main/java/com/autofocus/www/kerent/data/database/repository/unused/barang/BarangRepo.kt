package com.autofocus.www.kerent.data.database.repository.unused.barang

import io.reactivex.Observable

interface BarangRepo {

    fun isBarangRepoEmpty(): Observable<Boolean>

    fun insertBarang(barangs: List<Barang>): Observable<Boolean>

    fun loadBarang(): Observable<List<Barang>>
}