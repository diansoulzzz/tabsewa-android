package com.autofocus.www.kerent.data.database.repository.unused.carousel

import io.reactivex.Observable

interface CarouselRepo {

    fun isCarouselRepoEmpty(): Observable<Boolean>

    fun insertCarousel(carousels: List<Carousel>): Observable<Boolean>

    fun loadCarousel(): Observable<List<Carousel>>
}