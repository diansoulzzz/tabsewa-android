package com.autofocus.www.kerent.data.models

import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User(
        @Expose @PrimaryKey var id: Long,
        @Expose @SerializedName("name") var name: String?,
        @Expose @SerializedName("email") var email: String?,
        @Expose @SerializedName("foto_url") var fotoUrl: String?,
        @Expose @SerializedName("created_at") var createdAt: String?,
        @Expose @SerializedName("updated_at") var updatedAt: String?,
        @Expose @SerializedName("aktif") var aktif: Int?,
        @Expose @SerializedName("provider") var provider: String?,
        @Expose @SerializedName("provider_id") var providerId: String?,
        @Expose @SerializedName("verified") var verified: Int?,
        @Expose @SerializedName("no_ktp") var noKtp: String?,
        @Expose @SerializedName("foto_ktp") var fotoKtp: String?,
        @Expose @SerializedName("firebase_token") var firebaseToken: String?
)