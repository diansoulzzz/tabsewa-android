package com.autofocus.www.kerent.ui.menu.home.presenter

import com.autofocus.www.kerent.ui.base.presenter.MVPPresenter
import com.autofocus.www.kerent.ui.menu.home.interactor.HomeMVPInteractor
import com.autofocus.www.kerent.ui.menu.home.view.HomeMVPView


interface HomeMVPPresenter<V : HomeMVPView, I : HomeMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()
}