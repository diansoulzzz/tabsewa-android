package com.autofocus.www.kerent.ui.menu.register.interactor

import com.autofocus.www.kerent.data.network.ApiHelper
import com.autofocus.www.kerent.data.preferences.PreferenceHelper
import com.autofocus.www.kerent.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class RegisterInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), RegisterMVPInteractor {

//    override fun getBlogList() = apiHelper.getBlogApiCall()

}