package com.autofocus.www.kerent.ui.menu.register.presenter

import com.autofocus.www.kerent.ui.base.presenter.BasePresenter
import com.autofocus.www.kerent.ui.menu.register.interactor.RegisterMVPInteractor
import com.autofocus.www.kerent.ui.menu.register.view.RegisterMVPView
import com.autofocus.www.kerent.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RegisterPresenter<V : RegisterMVPView, I : RegisterMVPInteractor> @Inject constructor(interactor: I, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = compositeDisposable), RegisterMVPPresenter<V, I> {

    override fun onViewPrepared() {
        getView()?.showProgress()
//        interactor?.let {
//            it.getBlogList()
//                    .compose(schedulerProvider.ioToMainObservableScheduler())
//                    .subscribe { blogResponse ->
//                        getView()?.let {
//                            it.hideProgress()
//                            it.displayBlogList(blogResponse.data)
//                        }
//                    }
//        }
    }
}