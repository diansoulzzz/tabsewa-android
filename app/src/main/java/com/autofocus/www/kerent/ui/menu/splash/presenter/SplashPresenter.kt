package com.autofocus.www.kerent.ui.menu.splash.presenter

import com.autofocus.www.kerent.ui.base.presenter.BasePresenter
import com.autofocus.www.kerent.ui.menu.splash.interactor.SplashMVPInteractor
import com.autofocus.www.kerent.ui.menu.splash.view.SplashMVPView
import com.autofocus.www.kerent.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashPresenter<V : SplashMVPView, I : SplashMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), SplashMVPPresenter<V, I> {

    override fun onAttach(view: V?) {
        super.onAttach(view)
        feedInDatabase()
    }

    private fun feedInDatabase() = interactor?.let {
        compositeDisposable.add(it.seedQuestions()
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({
                    getView()?.let { decideActivityToOpen() }
                }))
    }

    private fun decideActivityToOpen() = getView()?.let {
        if (isUserLoggedIn())
            it.openMainActivity()
        else
            it.openLoginActivity()
    }

    private fun isUserLoggedIn(): Boolean {
        interactor?.let { return it.isUserLoggedIn() }
        return false
    }

}