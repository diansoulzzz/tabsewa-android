package com.autofocus.www.kerent.ui.menu.category.interactor

import com.autofocus.www.kerent.data.network.ApiHelper
import com.autofocus.www.kerent.data.preferences.PreferenceHelper
import com.autofocus.www.kerent.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class CategoryInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), CategoryMVPInteractor {

//    override fun getBlogList() = apiHelper.getBlogApiCall()

}