package com.autofocus.www.kerent.ui.menu.main.view

import com.autofocus.www.kerent.ui.base.view.MVPView

interface MainMVPView : MVPView {
    fun openLoginActivity()
}