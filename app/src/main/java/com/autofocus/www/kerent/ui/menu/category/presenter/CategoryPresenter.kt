package com.autofocus.www.kerent.ui.menu.category.presenter

import com.autofocus.www.kerent.ui.base.presenter.BasePresenter
import com.autofocus.www.kerent.ui.menu.category.interactor.CategoryMVPInteractor
import com.autofocus.www.kerent.ui.menu.category.view.CategoryMVPView
import com.autofocus.www.kerent.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CategoryPresenter<V : CategoryMVPView, I : CategoryMVPInteractor> @Inject constructor(interactor: I, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = compositeDisposable), CategoryMVPPresenter<V, I> {

    override fun onViewPrepared() {
        getView()?.showProgress()
//        interactor?.let {
//            it.getBlogList()
//                    .compose(schedulerProvider.ioToMainObservableScheduler())
//                    .subscribe { blogResponse ->
//                        getView()?.let {
//                            it.hideProgress()
//                            it.displayBlogList(blogResponse.data)
//                        }
//                    }
//        }
    }
}