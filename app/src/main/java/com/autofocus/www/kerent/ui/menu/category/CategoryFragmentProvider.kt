package com.autofocus.www.kerent.ui.menu.category

import com.autofocus.www.kerent.ui.menu.category.view.CategoryFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class CategoryFragmentProvider {

    @ContributesAndroidInjector(modules = [CategoryFragmentModule::class])
    internal abstract fun provideCategoryFragmentFactory(): CategoryFragment
}