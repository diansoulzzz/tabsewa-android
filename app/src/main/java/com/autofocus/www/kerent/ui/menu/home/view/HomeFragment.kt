package com.autofocus.www.kerent.ui.menu.home.view

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.autofocus.www.kerent.R
import com.autofocus.www.kerent.data.network.HomeResponse
import com.autofocus.www.kerent.ui.base.view.BaseFragment
import com.autofocus.www.kerent.ui.menu.home.interactor.HomeMVPInteractor
import com.autofocus.www.kerent.ui.menu.home.presenter.HomeMVPPresenter
import javax.inject.Inject

class HomeFragment : BaseFragment(), HomeMVPView {

    override fun displayHomeData(home: HomeResponse.Response) {
        Log.d("AppLogs",home.data.barang[0].nama)

    }

    companion object {

        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

//    @Inject
//    internal lateinit var HomeAdapter: HomeAdapter
//    @Inject
//    internal lateinit var layoutManager: LinearLayoutManager
    @Inject
    internal lateinit var presenter: HomeMVPPresenter<HomeMVPView, HomeMVPInteractor>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_home, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun setUp() {
//        layoutManager.orientation = LinearLayoutManager.VERTICAL
//        Home_recycler_view.layoutManager = layoutManager
//        Home_recycler_view.itemAnimator = DefaultItemAnimator()
//        Home_recycler_view.adapter = HomeAdapter
        presenter.onViewPrepared()
    }

//    override fun displayHomeList(Homes: List<Home>?) = Homes?.let {
//        HomeAdapter.addHomesToList(it)
//    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }
}