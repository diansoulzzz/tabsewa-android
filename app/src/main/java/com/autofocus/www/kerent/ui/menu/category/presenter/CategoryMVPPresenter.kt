package com.autofocus.www.kerent.ui.menu.category.presenter

import com.autofocus.www.kerent.ui.base.presenter.MVPPresenter
import com.autofocus.www.kerent.ui.menu.category.interactor.CategoryMVPInteractor
import com.autofocus.www.kerent.ui.menu.category.view.CategoryMVPView

interface CategoryMVPPresenter<V : CategoryMVPView, I : CategoryMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()
}