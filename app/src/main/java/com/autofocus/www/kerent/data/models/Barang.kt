package com.autofocus.www.kerent.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Barang(
        @Expose @SerializedName("id") var id: Int,
        @Expose @SerializedName("nama") var nama: String,
        @Expose @SerializedName("deskripsi") var deskripsi: String,
        @Expose @SerializedName("barang_sub_kategori_id") var barangSubKategoriId: Int,
        @Expose @SerializedName("users_id") var usersId: Int,
        @Expose @SerializedName("stok") var stok: Int,
        @Expose @SerializedName("created_at") var createdAt: String,
        @Expose @SerializedName("updated_at") var updatedAt: String,
        @Expose @SerializedName("deleted_at") var deletedAt: Any,
        @Expose @SerializedName("deposit_min") var depositMin: Int,
        @Expose @SerializedName("stok_available") var stokAvailable: Int,
        @Expose @SerializedName("stok_onbook") var stokOnbook: Int,
        @Expose @SerializedName("user") var user: User,
        @Expose @SerializedName("barang_foto_utama") var barangFotoUtama: Any,
        @Expose @SerializedName("barang_fotos") var barangFotos: List<Any>,
        @Expose @SerializedName("barang_sub_kategori") var barangSubKategori: BarangSubKategori
)