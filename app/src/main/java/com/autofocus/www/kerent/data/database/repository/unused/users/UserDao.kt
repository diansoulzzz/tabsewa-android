package com.autofocus.www.kerent.data.database.repository.unused.users

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(question: List<User>)

    @Query("SELECT * FROM users")
    fun loadAll(): List<User>
}