package com.autofocus.www.kerent.ui.menu.splash.interactor

import android.content.Context
import com.autofocus.www.kerent.data.database.repository.questions.Question
import com.autofocus.www.kerent.data.database.repository.questions.QuestionRepo
import com.autofocus.www.kerent.data.network.ApiHelper
import com.autofocus.www.kerent.data.preferences.PreferenceHelper
import com.autofocus.www.kerent.ui.base.interactor.BaseInteractor
import com.autofocus.www.kerent.util.AppConstants
import com.autofocus.www.kerent.util.FileUtils
import com.google.gson.GsonBuilder
import com.google.gson.internal.`$Gson$Types`
import io.reactivex.Observable
import javax.inject.Inject

class SplashInteractor @Inject constructor(private val mContext: Context, private val questionRepoHelper: QuestionRepo,  preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), SplashMVPInteractor {

    override fun getQuestion(): Observable<List<Question>> {
        return questionRepoHelper.loadQuestions()
    }

    override fun seedQuestions(): Observable<Boolean> {
        val builder = GsonBuilder().excludeFieldsWithoutExposeAnnotation()
        val gson = builder.create()
        return questionRepoHelper.isQuestionsRepoEmpty().concatMap { isEmpty ->
            if (isEmpty) {
                val type = `$Gson$Types`.newParameterizedTypeWithOwner(null, List::class.java, Question::class.java)
                val questionList = gson.fromJson<List<Question>>(
                        FileUtils.loadJSONFromAsset(
                                mContext,
                                AppConstants.SEED_DATABASE_QUESTIONS),
                        type)
                questionRepoHelper.insertQuestions(questionList)
            } else
                Observable.just(false)
        }
    }

}