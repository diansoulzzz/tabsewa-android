package com.autofocus.www.kerent.ui.menu.register

import android.support.v7.widget.LinearLayoutManager
import com.autofocus.www.kerent.ui.menu.register.interactor.RegisterInteractor
import com.autofocus.www.kerent.ui.menu.register.interactor.RegisterMVPInteractor
import com.autofocus.www.kerent.ui.menu.register.presenter.RegisterMVPPresenter
import com.autofocus.www.kerent.ui.menu.register.presenter.RegisterPresenter
import com.autofocus.www.kerent.ui.menu.register.view.RegisterMVPView
//import com.autofocus.www.kerent.ui.menu.Register.view.RegisterAdapter
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class RegisterFragmentModule {

    @Provides
    internal fun provideRegisterInteractor(interactor: RegisterInteractor): RegisterMVPInteractor = interactor

    @Provides
    internal fun provideRegisterPresenter(presenter: RegisterPresenter<RegisterMVPView, RegisterMVPInteractor>)
            : RegisterMVPPresenter<RegisterMVPView, RegisterMVPInteractor> = presenter

//    @Provides
//    internal fun provideRegisterAdapter(): RegisterAdapter = RegisterAdapter(ArrayList())

//    @Provides
//    internal fun provideLinearLayoutManager(fragment: RegisterFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)

}