package com.autofocus.www.kerent.ui.menu.main.interactor

import com.autofocus.www.kerent.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface MainMVPInteractor : MVPInteractor {
    fun getData()
}