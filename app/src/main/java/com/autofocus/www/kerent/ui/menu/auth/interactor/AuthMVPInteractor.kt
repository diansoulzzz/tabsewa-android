package com.autofocus.www.kerent.ui.menu.auth.interactor

import com.autofocus.www.kerent.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface AuthMVPInteractor : MVPInteractor {
    fun getData()
}