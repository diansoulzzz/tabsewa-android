package com.autofocus.www.kerent.ui.menu.login.presenter

import com.autofocus.www.kerent.ui.base.presenter.BasePresenter
import com.autofocus.www.kerent.ui.menu.login.interactor.LoginMVPInteractor
import com.autofocus.www.kerent.ui.menu.login.view.LoginMVPView
import com.autofocus.www.kerent.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginPresenter<V : LoginMVPView, I : LoginMVPInteractor> @Inject constructor(interactor: I, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = compositeDisposable), LoginMVPPresenter<V, I> {

    override fun onViewPrepared() {
        getView()?.showProgress()
//        interactor?.let {
//            it.getBlogList()
//                    .compose(schedulerProvider.ioToMainObservableScheduler())
//                    .subscribe { blogResponse ->
//                        getView()?.let {
//                            it.hideProgress()
//                            it.displayBlogList(blogResponse.data)
//                        }
//                    }
//        }
    }
}