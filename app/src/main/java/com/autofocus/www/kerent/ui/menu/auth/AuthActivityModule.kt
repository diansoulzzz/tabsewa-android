package com.autofocus.www.kerent.ui.menu.auth

import com.autofocus.www.kerent.ui.menu.auth.interactor.AuthInteractor
import com.autofocus.www.kerent.ui.menu.auth.interactor.AuthMVPInteractor
import com.autofocus.www.kerent.ui.menu.auth.presenter.AuthMVPPresenter
import com.autofocus.www.kerent.ui.menu.auth.presenter.AuthPresenter
import com.autofocus.www.kerent.ui.menu.auth.view.AuthMVPView
import dagger.Module
import dagger.Provides

@Module
class AuthActivityModule {
    @Provides
    internal fun provideAuthInteractor(AuthInteractor: AuthInteractor): AuthMVPInteractor = AuthInteractor

    @Provides
    internal fun provideAuthPresenter(AuthPresenter: AuthPresenter<AuthMVPView, AuthMVPInteractor>)
            : AuthMVPPresenter<AuthMVPView, AuthMVPInteractor> = AuthPresenter
}